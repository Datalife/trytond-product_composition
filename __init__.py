# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import compostion


def register():
    Pool.register(
        compostion.Composition,
        product.Product,
        compostion.Ingredient,
        compostion.CompositionIngredient,
        module='product_composition', type_='model')
