# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    composition = fields.Many2One('product.composition', 'Composition',
        select=True, ondelete='RESTRICT')
    ingredients_names = fields.Function(
        fields.Text('Ingredients names', translate=True),
        'get_ingredients_names')

    def get_ingredients_names(self, name=None):
        return self.composition.ingredients_names if self.composition else ''

    @fields.depends('composition', '_parent_composition.ingredients_names')
    def on_change_composition(self):
        if self.composition:
            self.ingredients_names = self.composition.ingredients_names
