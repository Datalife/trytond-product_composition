datalife_product_composition
============================

The product_composition module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-product_composition/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-product_composition)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
