# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool


class ProductCompositionTestCase(ModuleTestCase):
    """Test Product Composition module"""
    module = 'product_composition'

    def create_composition(self):
        pool = Pool()
        Ingredient = pool.get('product.composition.ingredient')
        Composition = pool.get('product.composition')

        ingredients = Ingredient.create([
            {'name': 'ingredient1'},
            {'name': 'ingredient2'},
            {'name': 'ingredient3'},
            {'name': 'ingredient4'}])

        return (Composition.create([{
            'name': 'Composition1',
            'ingredients': [('create', [
                {'ingredient': ingredients[0], 'percentage': 33},
                {'ingredient': ingredients[1], 'percentage': 33},
                {'ingredient': ingredients[2], 'percentage': 34}])]}])[0],
            ingredients)

    @with_transaction()
    def test_composition_crud(self):
        """Test composition CRUD"""
        pool = Pool()
        Composition = pool.get('product.composition')

        composition, _ = self.create_composition()
        Composition.write([composition], {'name': 'Composition2'})
        Composition.delete([composition])

    @with_transaction()
    def test_total_percent_less_than_one_hundred(self):
        """Test total_percent_less_than_one_hundred constraint"""
        pool = Pool()
        Composition = pool.get('product.composition')
        Ingredient = pool.get('product.composition.ingredient')

        ingredients = Ingredient.create([
            {'name': 'ingredient1'},
            {'name': 'ingredient2'},
            {'name': 'ingredient3'},
            {'name': 'ingredient4'}])

        self.assertRaises(Exception, Composition.create, [{
            'name': 'Composition1',
            'ingredients': [('create', [
                {'ingredient': ingredients[0], 'percentage': 33},
                {'ingredient': ingredients[1], 'percentage': 33},
                {'ingredient': ingredients[2], 'percentage': 35}])]
        }])

    @with_transaction()
    def test_total_percent_less_than_one_hundred2(self):
        """Test total_percent_less_than_one_hundred constraint"""
        pool = Pool()
        Composition = pool.get('product.composition')

        composition, ingredients = self.create_composition()
        self.assertRaises(Exception, Composition.write, [composition], {
            'ingredients': [('create', [{
                'ingredient': ingredients[3],
                'percentage': 1}])]})


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductCompositionTestCase))
    return suite
