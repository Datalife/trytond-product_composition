# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Unique, DeactivableMixin
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Composition(DeactivableMixin, ModelView, ModelSQL):
    """Product composition"""
    __name__ = 'product.composition'

    name = fields.Char('Name', required=True, translate=True)
    ingredients = fields.One2Many(
        'product.composition-product.composition.ingredient', 'composition',
        'Ingredients')
    ingredients_names = fields.Function(
        fields.Text('Ingredients names', translate=True),
        'get_ingredients_names')

    def get_ingredients_names(self, name, sep='\n'):
        return sep.join([i.rec_name for i in self.ingredients])

    @classmethod
    def validate(cls, records):
        super(Composition, cls).validate(records)
        for r in records:
            r.check_total_percentage()

    def check_total_percentage(self):
        pool = Pool()
        CompositionIngredient = \
            pool.get('product.composition-product.composition.ingredient')
        if not CompositionIngredient.check_total_percentage(self.ingredients):
            raise UserError(gettext('product_composition.'
                'msg_composition_total_percent_less_than_one_hundred',
                composition=self.rec_name))


class Ingredient(DeactivableMixin, ModelSQL, ModelView):
    """Product Composition"""
    __name__ = 'product.composition.ingredient'

    name = fields.Char('Name', required=True, translate=True)
    compositions = fields.One2Many(
        'product.composition-product.composition.ingredient', 'ingredient',
        'Compositions')


class CompositionIngredient(ModelSQL, ModelView):
    """Composition - Ingredient"""
    __name__ = 'product.composition-product.composition.ingredient'
    _table = 'product_composition_ingredient_rel'

    composition = fields.Many2One('product.composition', 'Composition',
        required=True, select=True, ondelete='CASCADE')
    ingredient = fields.Many2One('product.composition.ingredient',
        'Ingredient', required=True, select=True,
        ondelete='RESTRICT')
    percentage = fields.Float('Percentage', digits=(3, 3),
        domain=[('percentage', '>', 0), ('percentage', '<=', 100)])

    @classmethod
    def __setup__(cls):
        super(CompositionIngredient, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('uk1', Unique(t, t.composition, t.ingredient),
                'product_composition.msg_composition_ingredient_unique')]

    def get_rec_name(self, name=None):
        result = ''
        if self.percentage:
            mask = '%d %% ' if self.percentage == \
                int(self.percentage) else '%s %% '
            result = mask % self.percentage
        return result + self.ingredient.rec_name

    @classmethod
    def check_total_percentage(cls, c_ingredients):
        return not (sum(ci.percentage for ci in c_ingredients
            if ci.percentage) > 100)
